package com.accedo.compose_integration

data class HeroModel(
    val name: String,
    val realName: String?,
    val imageURL: String?,
    val company: ComicCompany
)

enum class ComicCompany {
    MARVEL
}
