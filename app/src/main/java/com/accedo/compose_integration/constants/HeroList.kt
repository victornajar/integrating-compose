package com.accedo.compose_integration.constants

import com.accedo.compose_integration.ComicCompany
import com.accedo.compose_integration.HeroModel

val heroList = listOf(
    HeroModel(
        name = "Ironman",
        realName = "Tonny Stark",
        imageURL = "https://cdn.iconscout.com/icon/free/png-256/ironman-marvel-super-hero-earth-saver-avenger-28699.png",
        company = ComicCompany.MARVEL
    ),
    HeroModel(
        name = "Ant-Man",
        realName = "Scott Lang",
        imageURL = "https://i.pinimg.com/originals/72/9c/72/729c7266379cb5c204fe5b167341fd16.png",
        company = ComicCompany.MARVEL
    ),
    HeroModel(
        name = "Captain America",
        realName = "Steve Rogers",
        imageURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Captain_America%27s_shield.svg/2048px-Captain_America%27s_shield.svg.png",
        company = ComicCompany.MARVEL
    )
)