package com.accedo.compose_integration

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.accedo.compose_integration.databinding.LayoutHeroItemBinding
import com.bumptech.glide.Glide
import java.lang.ClassCastException

class HeroListAdapter(
    private val heroes: List<HeroModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val MARVEL_HERO = 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (heroes[position].company) {
            ComicCompany.MARVEL -> MARVEL_HERO
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            MARVEL_HERO -> {
                val binding = LayoutHeroItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )

                MarvelHeroViewHolder(binding)
            }
            else -> throw ClassCastException("No support for that hero type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MarvelHeroViewHolder -> {
                val hero = heroes[position]
                holder.bind(hero)
            }
        }
    }

    override fun getItemCount() = heroes.size

    inner class MarvelHeroViewHolder(
        private val binding: LayoutHeroItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(hero: HeroModel) {
            binding.tvHeroName.text = hero.name
            binding.tvHeroRealName.text = hero.realName

            Glide.with(itemView.context)
                .load(hero.imageURL)
                .centerCrop()
                .circleCrop()
                .into(binding.ivHero)
        }

    }

}
